import React from 'react';

export default ({temp, location}) => (
  <p>It's {temp} in {location}</p>
);
