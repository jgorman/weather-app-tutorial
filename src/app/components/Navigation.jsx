import React from 'react';
import {IndexLink, Link} from 'react-router';

const bold = {fontWeight: 'bold'};

export default (props) => (
  <div>
    <h1>Navigation</h1>
    <IndexLink to="/" activeClassName="active" activeStyle={bold}>Get Weather</IndexLink>
    <Link to="/about" activeClassName="active" activeStyle={bold}>About</Link>
    <Link to="/examples" activeClassName="active" activeStyle={bold}>Examples</Link>
  </div>
);
