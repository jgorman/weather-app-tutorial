import React from 'react';
import Navigation from 'components/Navigation';

export default React.createClass({
  render() {
    return (
      <div>
        <h2>Main component</h2>
        <Navigation/>
        {this.props.children}
      </div>
    );
  }
});
