import React from 'react';

export default React.createClass({
  onSubmit(e) {
    e.preventDefault();

    const location = this.refs.location.value;

    if (location) {
      this.refs.location.value = '';
      this.props.updateLocation(location);
    }
  },
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <input type="text" ref="location"/>
        <button>Get Weather</button>
      </form>
    );
  }
});
