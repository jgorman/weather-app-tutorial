import React from 'react';
import GetWeather from 'components/GetWeather';
import WeatherMessage from 'components/WeatherMessage';
import openWeatherMap from 'api/openWeatherMap';

export default React.createClass({
  getInitialState() {
    return {
      location: 'Nowhere',
      temp: null,
      loading: false
    };
  },
  updateLocation(location) {
    this.setState({loading: true});
    openWeatherMap.get(location)
      .then(res =>
        this.setState({
          location: res.location,
          temp: res.temp,
          error: null,
          loading: false
        })
      )
      .catch(err =>
        this.setState({
          location: location,
          temp: null,
          error: err.message,
          loading: false
        })
      );
  },
  render() {
    const {location, temp, error, loading} = this.state;

    const message = () => {
      if (loading) {
        return (<p>Loading...</p>);
      } else if (error) {
        return (<p>{error} - {location}</p>)
      } else if (temp) {
        return (<WeatherMessage location={location} temp={temp}/>);
      }
      return null;
    };

    return (
      <div>
        <GetWeather updateLocation={this.updateLocation}/>
        {message()}
      </div>
    );
  }
});
