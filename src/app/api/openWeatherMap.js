import axios from 'axios';

const baseUrl = 'http://api.openweathermap.org/data/2.5/weather?appid=c5a0c16e10baf2d292d49acbb921ad58';

export default {
  get(location) {
    const url = `${baseUrl}&q=${encodeURIComponent(location)}&units=metric`;

    return axios.get(url)
      .then(res => {
        if (res.data.cod && res.data.message) {
          throw new Error(res.data.message);
        } else if (res.data.main.temp && res.data.name) {
          return {
            location: res.data.name,
            temp: res.data.main.temp
          };
        }
        throw new Error("Missing temparature from response");
      },
      e => {
        throw new Error(e.data.message);
      });
  }
};
