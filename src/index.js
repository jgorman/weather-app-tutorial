import express from 'express';

// Create our app
const app = express();
const port = process.env.PORT || 3000;

app.use((req, res, next) => {
  if (req.protocol === 'http') {
    next();
  } else {
    res.redirect(`http://${req.hostname}${req.url}`);
  }
});

app.use(express.static('bin/public'));

app.listen(port, function () {
  console.log(`Express server is up on port ${port}`);
});
