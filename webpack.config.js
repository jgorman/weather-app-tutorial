var path = require('path');

module.exports = {
  entry: {
    javascript: './src/app/app.jsx',
    html:  './src/public/index.html'
  },
  output: {
    path: __dirname + '/bin/public',
    filename: 'bundle.js'
  },
  resolve: {
    root: path.join(__dirname, 'src/app'),
    alias: {
    },
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: "file?name=[name].[ext]",
        test: /\.html$/,
      },
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  devtool: 'cheap-module-eval-source-map'
};
